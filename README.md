# Complete Caffe_Classification Guide "Train Model with Own Dataset"


##### Installing Graphics Dependencies

```
$ pip install pydot
$ sudo apt-get install graphviz libgraphviz-dev
$ pip install pygraphviz
```

##### Creating a simple layer

Create a python file and add the following lines:
```
import sys
import numpy as np
import matplotlib.pyplot as plt
sys.insert('/path/to/caffe/python')
import caffe
```

If you have a GPU onboard, then we need to tell Caffe that we want it to use the GPU:

```
caffe.set_device(0)
caffe.set_mode_gpu()
```


##### Creating Network Layer
Creating a simple layer: Name ConvNet.prototxt
```
name:"convolutionlayer1"
input: "data"
input_dim: 1
input_dim: 1
input_dim: 256
input_dim: 256
layer {
  name: "conv"
  type: "Convolution"
  bottom: "data"
  top: "conv"
  convolution_param {
    num_output: 5
    kernel_size: 3
    stride: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
```
In the above layer we have 5 classes as output, neuron of size 383 and a stride (the shift value of the kernel/filter/neuron) of 1.

##### Graphical Representation of the Network

A simple command can be used to output the graphical representation of network as shown below:


```
$ python /path/to/caffe/python/draw_net.py myconvnet.prototxt myconvnet.png
```
-----------------------------------------------------------------------------------------------------------------------------------
##### Working with the Network Model Created

Caffe has two prototxt files it works it, 1. Network file 2. Solver file. I have already developed a single layer prototxt file. Prototxt 
file defines the CNN architecture while solver file defines the training parameters. 

To run the network created:

```
import sys
sys.path.insert(0, '/path/to/caffe/python')
import caffe
import cv2
import numpy as np

net = caffe.Net('myconvnet.prototxt', caffe.TEST)

print "\nnet.inputs =", net.inputs
print "\ndir(net.blobs) =", dir(net.blobs)
print "\ndir(net.params) =", dir(net.params)
print "\nconv shape = ", net.blobs['conv'].data.shape
```

##### Generating Network Output
```
img = cv2.imread('input_image.jpg', 0)
img_blobinp = img[np.newaxis, np.newaxis, :, :]
net.blobs['data'].reshape(*img_blobinp.shape)
net.blobs['data'].data[...] = img_blobinp

net.forward()

for i in range(10):
    cv2.imwrite('output_image_' + str(i) + '.jpg', 255*net.blobs['conv'].data[0,i])
```
##### Saving Network Output
```
net.save('ConvNet.caffemodel')
```

This saves the model for reuse which prevents re-creating from scratch.

--------------------------------------------------------------------------------------------------------------------------------






